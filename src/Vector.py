import math


def dst(v1, v2):
    return math.sqrt((v1.x - v2.x) ** 2 + (v1.y - v2.y) ** 2)


def subtract(n1, n2):
    return Vector2(n1.x - n2.x, n1.y - n2.y)


class Vector2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def multiply(self, scalar):
        self.x *= scalar
        self.y *= scalar

    def add(self, other):
        self.x += other.x
        self.y += other.y

    def set(self, x, y):
        self.x = x
        self.y = y

    def limit(self, length):
        if self.length() > length:
            if self.x == 0:
                self.y = length
                return
            elif self.y == 0:
                self.x = length
                return
            elif self.x == 0 and self.y == 0:
                return
            else:
                a = (length**2)/(self.x**2+self.y**2)
                self.x *= a
                self.y *= a

    def normalized(self):
        l = self.length()
        if l == 0:
            return Vector2(0, 0)
        return Vector2(self.x / l, self.y / l)

    def __str__(self):
        return str(self.x) + " " + str(self.y)

    def __mul__(self, other):
        return Vector2(self.x * other, self.y * other)
