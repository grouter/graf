from src.Edge import Edge
import pygame
from src.Vector import Vector2
import random

dumping = 0.8
velocity_limit = 15


class Node:
    def __init__(self, data, pos=None):

        if pos:
            self.pos = pos
        else:
            self.pos = Vector2(random.randrange(0, 900), random.randrange(0, 900))

        self.width, self.height = pygame.display.get_surface().get_size()

        self.data = data
        self.edges = []
        self.size = 10
        self.velocity = Vector2(0, 0)
        self.text = pygame.font.SysFont("DejaVu", 15).render(str(self.data), True, (150, 0, 0))
        self.color = (255, 255, 255)

    def connect(self, other):
        if other == self:
            raise Exception("Cannot connect to self")
        self.edges.append(Edge(self, other))

    def connected_to(self, other):
        for edge in self.edges:
            if edge.connected_between(self, other):
                return True
        return False

    def remove_edge(self, node):
        self.edges.remove(node)

    def apply_force(self, force):
        self.velocity.add(force)

    def update(self, delta):
        self.apply_force(Vector2(self.width/2 - self.pos.x, self.height/2 - self.pos.y).normalized() * 0.5)  # move towards center
        self.velocity.multiply(dumping)
        self.velocity.limit(velocity_limit)
        self.pos.add(self.velocity * delta)

        # limiting screen space
        self.pos.x = max(0, min(self.pos.x, self.width))
        self.pos.y = max(0, min(self.pos.y, self.height))

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (int(self.pos.x), int(self.pos.y)), self.size)
        screen.blit(self.text, (self.pos.x - self.text.get_width() // 2, self.pos.y - self.text.get_height() // 2))

    def draw_edges(self, screen):
        for edge in self.edges:
            edge.draw(screen)

    def get_data(self):
        return self.data

    def get_edge_count(self):
        return len(self.edges)

    def get_edges(self):
        return self.edges

    def get_edge_data(self):
        return [n.get_data() for n in self.get_neighbors()]

    def get_pos(self):
        return self.pos

    def get_size(self):
        return self.size

    def get_color(self):
        return self.color

    def get_neighbors(self):
        return [n.get_other(self) for n in self.edges]

    def set_pos(self, x, y):
        if self.pos:
            self.pos.set(x, y)
            self.velocity.multiply(0)
        else:
            self.pos = Vector2(x, y)

    def set_col(self, new_col):
        self.color = new_col
