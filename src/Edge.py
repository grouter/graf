import pygame


class Edge:
    def __init__(self, node1, node2):
        self.pair = [node1, node2]

    # preventing from connecting to self, so this should be ok
    def connected_between(self, n1, n2):
        return (self.pair[0] == n1 and self.pair[1] == n2) or (self.pair[0] == n2 and self.pair[1] == n1)

    def get_other(self, node):
        return [n for n in self.pair if n != node][0]

    def draw(self, screen):
        p1 = self.pair[0].get_pos()
        p2 = self.pair[1].get_pos()
        pygame.draw.line(screen, (0, 0, 0), (p1.x, p1.y), (p2.x, p2.y))

    def __eq__(self, other):
        return (self.pair[0] == other.pair[0] and self.pair[1] == other.pair[1]) or (self.pair[0] == other.pair[1] and self.pair[1] == other.pair[0])